#!/bin/sh
echo Configureing Virtual Reality
wait 3
mono bin/Prebuild.exe /target vs2010 /targetframework v4_0 /conditionals NET_4_0 LINUX
if [ -d ".git" ]; then git log --pretty=format:"VirtualReality:LiveBeta:%h)" -n 1 > bin/.version; fi
wait 3
echo Building Virtual Reality 
xbuild /property:DefineConstants="LINUX NET_4_0"
echo Finished Building Virtual Reality
echo Thank you for choosing Virtual Reality
echo Please report any errors to our Project Tracker http://projects.virtualrealitygrid.net
wait 5